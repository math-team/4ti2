Description: upstream autotool machinery: public shared scheme
 Formally this patch enforced the implicitly private shared scheme present
 in the upstream material. Now this patch renders this scheme public. This
 allows one to provide development packages (see #1012530).
Origin: debian
Forwarded: yes, by email to the `4ti2 Team'
Author: Jerome Benoit <calculus@rezozer.net>
Last-Update: 2022-08-21

--- a/src/4ti2/Makefile.am
+++ b/src/4ti2/Makefile.am
@@ -18,13 +18,16 @@
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
 
-fortytwoincludedir = $(includedir)/4ti2
+pkglibbindir = $(pkglibexecdir)/bin
+pkgliblibdir = $(libdir)
+
+fortytwoincludedir = $(pkgincludedir)/4ti2
 
 fortytwoinclude_HEADERS = 4ti2.h 4ti2xx.h 4ti2_config.h
 
 BUILT_SOURCES = 4ti2_config.h
 
-lib_LTLIBRARIES = lib4ti2common.la
+pkgliblib_LTLIBRARIES = lib4ti2common.la
 
 # Allow accessing zsolve's header files as "zsolve/HEADER.h"
 AM_CXXFLAGS = -I$(srcdir)/..
@@ -37,4 +40,5 @@
 endif
 
 lib4ti2common_la_LDFLAGS = $(AM_LDFLAGS) -no-undefined
+lib4ti2common_la_LDFLAGS += -version-info $(XLII_LT_VERSION)
 
--- a/src/groebner/Makefile.am
+++ b/src/groebner/Makefile.am
@@ -18,16 +18,18 @@
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
 
-bin_PROGRAMS = 
-bin_PROGRAMS += 4ti2int32 4ti2int64
+pkglibbindir = $(pkglibexecdir)/bin
+pkgliblibdir = $(libdir)
+
+pkglibbin_PROGRAMS =
+pkglibbin_PROGRAMS += 4ti2int32 4ti2int64
 if HAVE_GMP_WITH_CXX
-bin_PROGRAMS += 4ti2gmp
+pkglibbin_PROGRAMS += 4ti2gmp
 endif
 
-lib_LTLIBRARIES = 
-lib_LTLIBRARIES += lib4ti2int32.la lib4ti2int64.la
+pkgliblib_LTLIBRARIES = lib4ti2int32.la lib4ti2int64.la
 if HAVE_GMP_WITH_CXX
-lib_LTLIBRARIES += lib4ti2gmp.la
+pkgliblib_LTLIBRARIES += lib4ti2gmp.la
 endif
 
 # Allow accessing groebner's header files as "groebner/HEADER.h"
@@ -123,7 +125,7 @@
 	WeightedBinomialSet.cpp			\
 	WeightedReduction.cpp
 
-groebnerincludedir=$(includedir)/groebner
+groebnerincludedir=$(pkgincludedir)/groebner
 
 groebnerinclude_HEADERS =			\
 	Algorithm.h				\
@@ -268,7 +270,8 @@
 nodist_bin_SCRIPTS = $(WRAPPERSCRIPTS)
 DISTCLEANFILES = $(WRAPPERSCRIPTS)
 
-AM_LDFLAGS = -L../4ti2 -R$(libdir) -l4ti2common -no-undefined
+AM_LDFLAGS = -L../4ti2 -l4ti2common -no-undefined
+AM_LDFLAGS += -version-info $(XLII_LT_VERSION)
 
 # 16 bit precision flags.
 # 4ti2int16_LDADD = lib4ti2int16.la
--- a/src/util/Makefile.am
+++ b/src/util/Makefile.am
@@ -19,15 +19,19 @@
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
 
+pkglibbindir = $(pkglibexecdir)/bin
+pkgliblibdir = $(libdir)
+
 bin_PROGRAMS =					\
 	genmodel				\
 	gensymm					\
 	output
 #	normaliz_wrapper
 
-lib_LTLIBRARIES = lib4ti2util.la
+pkgliblib_LTLIBRARIES = lib4ti2util.la
 
 AM_LDFLAGS = -no-undefined
+AM_LDFLAGS += -version-info $(XLII_LT_VERSION)
 
 # Allow accessing general 4ti2 headers
 AM_CFLAGS = -I$(srcdir)/..
@@ -45,7 +49,7 @@
 	print.c					\
 	vector.c
 
-utilincludedir=$(includedir)/util
+utilincludedir=$(pkgincludedir)/util
 
 utilinclude_HEADERS = 				\
 	genmodel.h				\
--- a/src/zsolve/Makefile.am
+++ b/src/zsolve/Makefile.am
@@ -18,9 +18,12 @@
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
 
+pkglibbindir = $(pkglibexecdir)/bin
+pkgliblibdir = $(libdir)
+
 bin_PROGRAMS = zsolve 
 
-lib_LTLIBRARIES = libzsolve.la
+pkgliblib_LTLIBRARIES = libzsolve.la
 
 # Allow accessing zsolve's header files as "zsolve/HEADER.h"
 AM_CXXFLAGS = -I$(srcdir)/..
@@ -42,7 +45,7 @@
 	RelAPI.cpp \
 	SignAPI.cpp
 
-zsolveincludedir = $(includedir)/zsolve
+zsolveincludedir = $(pkgincludedir)/zsolve
 
 zsolveinclude_HEADERS = \
 	Integer.h \
@@ -88,7 +91,8 @@
 # Link in the "common" 4ti2 functions.
 # -no-undefined declares that no undefined symbols will remain after linking all these libraries.
 # (This is necessary to build shared libraries on Cygwin.)
-libzsolve_la_LDFLAGS = -L../4ti2 -R$(libdir) -l4ti2common ${GMP_LIBS} -no-undefined
+libzsolve_la_LDFLAGS = -L../4ti2 -l4ti2common ${GMP_LIBS} -no-undefined
+libzsolve_la_LDFLAGS += -version-info $(XLII_LT_VERSION)
 
 bin_SCRIPTS = hilbert graver
 DISTCLEANFILES = hilbert graver
--- a/src/groebner/script.template.in
+++ b/src/groebner/script.template.in
@@ -20,9 +20,12 @@
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
 
+prefix=@prefix@
+PKGLIBEXECDIR=@libexecdir@/@PACKAGE@
+
 # We locate where this script is so we can call the executables.
 SCRIPT=`which "$0"`
-DIR=`dirname "$SCRIPT"`
+SCRIPTDIR=`dirname "$SCRIPT"`
 FUNCTION=`basename "$SCRIPT"`
 
 # The default executable.
@@ -50,8 +53,12 @@
     EXECUTABLE=4ti2gmp
 fi
 
+for DIR in "$SCRIPTDIR" "$PKGLIBEXECDIR/bin"; do
+	if [ -x "$DIR/@GROEBNER_DEFAULT_EXECUTABLE@" ]; then break; fi
+done
+
 # We check whether the 4ti2 executable exists.
-if [ ! -f "$DIR/$EXECUTABLE" ]
+if [ ! -x "$DIR/$EXECUTABLE" ]
 then
     echo "Error: Unable to find 4ti2 executable \`$EXECUTABLE'"
     exit 1
--- /dev/null
+++ b/src/zsolve/graver.template.in
@@ -0,0 +1,24 @@
+#!/bin/sh
+
+prefix=@prefix@
+PKGLIBEXECDIR=@libexecdir@/@PACKAGE@
+
+# We locate where this script is so we can call the executable zsolve which
+# should be in the same directory as this script.
+SCRIPT=`which "$0"`
+SCRIPTDIR=`dirname "$SCRIPT"`
+EXECUTABLE=zsolve
+
+for DIR in "$SCRIPTDIR" "$PKGLIBEXECDIR/bin"; do
+	if [ -x "$DIR/$EXECUTABLE" ]; then break; fi
+done
+
+# Check if zsolve executable exists.
+if [ ! -f "$DIR/$EXECUTABLE" ]
+then
+    echo "Error: Unable to find the executable \`$EXECUTABLE'."
+    echo "Error: It should have been in the directory \`$DIR'."
+    exit 1
+fi
+
+"$DIR"/$EXECUTABLE -G $@
--- /dev/null
+++ b/src/zsolve/hilbert.template.in
@@ -0,0 +1,24 @@
+#!/bin/sh
+
+prefix=@prefix@
+PKGLIBEXECDIR=@libexecdir@/@PACKAGE@
+
+# We locate where this script is so we can call the executable zsolve which
+# should be in the same directory as this script.
+SCRIPT=`which "$0"`
+SCRIPTDIR=`dirname "$SCRIPT"`
+EXECUTABLE=zsolve
+
+for DIR in "$SCRIPTDIR" "$PKGLIBEXECDIR/bin"; do
+	if [ -x "$DIR/$EXECUTABLE" ]; then break; fi
+done
+
+# Check if zsolve executable exists.
+if [ ! -f "$DIR/$EXECUTABLE" ]
+then
+    echo "Error: Unable to find the executable \`$EXECUTABLE'."
+    echo "Error: It should have been in the directory \`$DIR'."
+    exit 1
+fi
+
+"$DIR"/$EXECUTABLE -H $@
--- a/configure.ac
+++ b/configure.ac
@@ -12,9 +12,23 @@
 		 doc/Makefile
 		 m4/Makefile])
 
+
+dnl Library versioning (C:R:A == current:revision:age)
+dnl See https://www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html
+dnl See <GSL>/configure.ac
+XLII_CURRENT=0
+XLII_REVISION=0
+XLII_AGE=0
+
+XLII_LT_VERSION="${XLII_CURRENT}:${XLII_REVISION}:${XLII_AGE}"
+AC_SUBST(XLII_LT_VERSION)
+
+
 AC_CONFIG_HEADERS([src/config.h src/4ti2/4ti2_config.h])
 
 AC_CONFIG_FILES([src/groebner/script.template])
+AC_CONFIG_FILES([src/zsolve/graver.template])
+AC_CONFIG_FILES([src/zsolve/hilbert.template])
 
 AC_CONFIG_FILES([test/Makefile])
 AC_CONFIG_FILES([test/circuits/Makefile])
